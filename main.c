#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>
#include "objs.h"

int
main()
{
    initscr();
    cbreak();
    noecho();
    curs_set(FALSE);
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE); /* getch() doesn't wait for input but acts when there is input */

    int maxy, maxx;
    getmaxyx(stdscr, maxy, maxx);

    struct objs SLIDER = {ACS_CKBOARD, FALSE, maxy - 1, 0};
    struct objs BALL = {'o', FALSE, 0, 0};

    int collision;
    char points[MAX_STRLEN];
    init_bricks(); 
    while (TRUE) {
        getmaxyx(stdscr, maxy, maxx);
        move_slider(maxx, &SLIDER);
        
        usleep(DELAY);
        clear();
        move_ball(&BALL);
        
        if ((collision = collide_slider(&SLIDER, &BALL)) == COLLISION_Y) {
            bounce(collision);
        } else if ((collision = collide(maxy, maxx, &BALL)) != QUIT
                && collision != NO_COLLISION) {
            bounce(collision);
        } else if (collision == NO_COLLISION) {
            ;
        } else if (collision == QUIT) {
            break;
        }
        

        draw_bricks();
        draw_slider(&SLIDER);

        sprintf(points, "POINTS:%d", (NBRICKS - number_of_bricks()) * 10);
        mvaddstr(0,0, points);
        refresh();

    }


    endwin();
}
