#if !defined(_BRICK_BREAKER)

#define _BRICK_BREAKER

#define DELAY 30000
#define SLIDER_LENGTH 80 
#define NBRICKS 100
#define MAX_STRLEN 80

#define COLLISION_BRICK 1
#define COLLISION_X 2
#define COLLISION_Y 3
#define QUIT 4
#define NO_COLLISION 0
 
extern struct objs *head;
extern int dy, dx;

struct objs {
    int icon;
    int breakable;
    int y;
    int x;
    struct objs *next;
};

/* frontend.c */
void draw_slider(struct objs *slider);
void draw_bricks();
void move_ball(struct objs *ball);


/* backend.c */
void bounce(int type);
int move_slider(int maxx, struct objs *slider);
int collide_slider(const struct objs *ball, const struct objs *slider);
void init_bricks();
int collide(const int maxy, const int maxx, const struct objs *ball);
int number_of_bricks();


#endif
